pipeline {
    agent any

    environment {
        DOCKER_BIN = 'docker'
        DOCKER_IMAGE = 'nelzer95/jk-flask-auth-app'
        IMAGE_TAG = 'latest'
        REGISTRY_CREDENTIALS_ID = 'dockerhub-login'
        HEROKU_APP_NAME = 'jk-flask'
        HEROKU_API_KEY_ID = 'heroku-api-key'
    }

    stages {
        stage('Build image') {
            steps {
                script {
                    echo "Building Docker image..."
                    sh "${DOCKER_BIN} build -t ${DOCKER_IMAGE}:${IMAGE_TAG} ."
                }
            }
        }

        stage('Push image to Registry') {
            steps {
                script {
                    echo "Logging into Docker Hub and pushing image..."
                    withCredentials([usernamePassword(credentialsId: REGISTRY_CREDENTIALS_ID, usernameVariable: 'REGISTRY_USER', passwordVariable: 'REGISTRY_PASS')]) {
                        sh "echo $REGISTRY_PASS | ${DOCKER_BIN} login --username $REGISTRY_USER --password-stdin"
                        sh "${DOCKER_BIN} push ${DOCKER_IMAGE}:${IMAGE_TAG}"
                    }
                }
            }
        }

        stage('Deploy to Heroku') {
            steps {
                script {
                    echo "Deploying to Heroku..."
                    withCredentials([string(credentialsId: HEROKU_API_KEY_ID, variable: 'HEROKU_API_KEY')]) {
                        sh "${DOCKER_BIN} tag ${DOCKER_IMAGE}:${IMAGE_TAG} registry.heroku.com/${HEROKU_APP_NAME}/web"
                        sh "echo $HEROKU_API_KEY | ${DOCKER_BIN} login --username=_ --password-stdin registry.heroku.com"
                        sh "${DOCKER_BIN} push registry.heroku.com/${HEROKU_APP_NAME}/web"
                        sh "heroku container:release web --app ${HEROKU_APP_NAME}"
                    }
                }
            }
        }

        stage('Check Deployment') {
            steps {
                script {
                    int attempts = 0
                    while (attempts < 10) {
                        try {
                            echo "Checking deployment..."
                            sh "curl -s https://jk-flask-c8abbd7ee4b6.herokuapp.com/login?next=%2F | grep Login"
                            break
                        } catch (Exception e) {
                            attempts++
                            if (attempts >= 10) {
                                echo "Deployment check failed after multiple attempts."
                                throw new Exception("Deployment content check failed.")
                            }
                            echo "Waiting for deployment to be ready..."
                            sleep(time: 30, unit: 'SECONDS')
                        }
                    }
                }
            }
        }
    }

    post {
        always {
            echo "Sending build completion email..."
            emailext(
                to: 'carnelilordwiz@gmail.com',
                subject: "Build ${currentBuild.fullDisplayName} completed",
                body: """<html><body>
                         <p>The build of <strong>${env.JOB_NAME}</strong> build number <strong>${env.BUILD_NUMBER}</strong> was completed.</p>
                         <p>Status: <strong>${currentBuild.currentResult}</strong></p>
                         <p>View build details at <a href='${BUILD_URL}'>${BUILD_URL}</a>.</p>
                         </body></html>""",
                mimeType: 'text/html'
            )
        }
        failure {
            echo "Build or deployment failed."
            emailext(
                to: 'carnelilordwiz@gmail.com',
                subject: "Build ${currentBuild.fullDisplayName} failed",
                body: """<html><body>
                         <p>The build of <strong>${env.JOB_NAME}</strong> build number <strong>${env.BUILD_NUMBER}</strong> failed.</p>
                         <p>Status: <strong>${currentBuild.currentResult}</strong></p>
                         <p>View build details at <a href='${BUILD_URL}'>${BUILD_URL}</a>.</p>
                         </body></html>""",
                mimeType: 'text/html'
            )
        }
    }
}
