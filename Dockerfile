FROM python:3.9-slim


WORKDIR /usr/src/app

COPY . .


RUN pip install --no-cache-dir -r requirements.txt


EXPOSE $PORT


ENV FLASK_APP=main.py
ENV FLASK_RUN_HOST=0.0.0.0


CMD flask run --host=0.0.0.0 --port=$PORT
